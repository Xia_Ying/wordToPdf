package com.demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;

public class TestWord {

    private static InputStream license;
    private static InputStream fileInput;
    private static File outputFile;

    public static boolean getLicense() {
        boolean result = false;
        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
//            license = new FileInputStream(loader.getResource("license.xml").getPath());// 凭证文件
            license = new FileInputStream(loader.getResource("license.xml").getPath());// 凭证文件
//            FileInputStream fileInputStream = new FileInputStream(new File( loader.getResource("license.xml")));
            fileInput = new FileInputStream(loader.getResource("D:\\outFiles\\word\\3538\\3538_2033199_10670798_2018_4_18.doc").getPath());// 待处理的文件
//            fileInput = new FileInputStream(loader.getResource("test.docx").getPath());// 待处理的文件
            outputFile = new File("d:/outFiles/321.pdf");// 输出路径
//            outputFile = new File("D:\\test.pdf");// 输出路径

            License aposeLic = new License();
            aposeLic.setLicense(license);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        // 验证License
        if (!getLicense()) {
            return;
        }
        try {
            long old = System.currentTimeMillis();
            Document doc = new Document(fileInput);
            FileOutputStream fileOS = new FileOutputStream(outputFile);

            doc.save(fileOS, SaveFormat.PDF);

            long now = System.currentTimeMillis();
            System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒\n\n" + "文件保存在:" + outputFile.getPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}